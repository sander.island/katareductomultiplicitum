import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        // Some tests
        System.out.println("sumDigProd(16, 28), should be 6:");
        sumDigProd(16,28);
        System.out.println();
        System.out.println("sumDigProd(9, 8), should be 7:");
        sumDigProd(9,8);
        System.out.println();
        System.out.println("sumDigProd(1, 2, 3, 4, 5, 6), should be 2:");
        sumDigProd(1,2,3,4,5,6);
        System.out.println();
        System.out.println("sumDigProd(26, 497, 62, 841), should be 6:");
        sumDigProd(26,497,62,841);
        System.out.println();
        System.out.println("sumDigProd(1111111111), should be 1:");
        sumDigProd(1111111111);
        System.out.println();
        System.out.println("sumDigProd(17737, 98723, 2), should be 6:");
        sumDigProd(17737,98723,2);
        System.out.println();
        System.out.println("sumDigProd(8, 16, 89, 3), should be 6:");
        sumDigProd(8,16,89,3);
    }

    public static void sumDigProd(int ... numbers) {
        int result = 0;

        // Adds the numbers
        for (int i = 0; i < numbers.length; i++) {
            result += numbers[i];
        }

        while (result >= 10) {  // While the result is more than a single digit number
            String[] resultString = Integer.toString(result).split("");
            ArrayList<String> digits = new ArrayList<>(Arrays.asList(resultString));

            for (int i = 0; i < digits.size()-1; i++) {
                if (i == 0) {
                    result = Integer.parseInt(digits.get(i)) * Integer.parseInt(digits.get(i+1));
                } else {
                    result = result * Integer.parseInt(digits.get(i+1));
                }
            }
        }
        System.out.println(result);
    }
}
